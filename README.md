Sketchpad Databroker Example Application
=========================================

This application is an example of what the SketchPad will build when creating databrokers and attaching databrokers to widgets.

The application has taken a scenario from Aviarc Air. The user selects an origin airport and a destination airport. The airport options are loaded from datasets. However, the destination airport options must not conatin the airport selected in the origin airport options.

There is one screen called ```airport-select```. It contains two select-lists and each list loads data from its own dataset The datasets are ```origin-airports``` and ```destination-airports``` respectfully. Both datasets are created from the same databroker called ```airports```.

The ```airports``` databroker is a file-backed databroker. The SketchPad will only created file-backed databrokers because they don't require a database and are they easy for developers to edit (they read from a human-readable XML file).
The ```airports``` databroker is able to filter it's results by means of a ```choose``` statement in it's workflows. The SketchPadd will automatically construct these ```choose``` statements based on the data-editing menus (to be created by jaro).